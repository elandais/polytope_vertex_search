#!/usr/bin/env python3
import rospy
from sensor_msgs.msg import JointState, PointCloud 
from geometry_msgs.msg import Polygon, Point32, PolygonStamped
from jsk_recognition_msgs.msg import PolygonArray
from std_msgs.msg import Header,Float64
from visualization_msgs.msg import Marker

# robot module import
import urdf_solver as panda
# time evaluation
import time
#arguments
import sys

#some math
import numpy as np

import capacity.robot_solver as capRob

base_link = "torso"
end_link = "end_effector_l"

#chosenSolver = capRob.URDFSolverKDL("/human_description",base_link,end_link)

urdf_path = "/home/elandais/catkin_ws/src/human_control/description/two_arms_only_marks/two_arms_only_marks_aCol.urdf.xacro"
chosenSolver = capRob.URDFSolverPIN(urdf_path,base_link,end_link)

#kdlSolver = panda.URDFSolverKDL("/human_description",base_link,end_link)

namespace = rospy.get_namespace()

joint_positions = [0 for k in range( len(chosenSolver.joint_name_list) )]

data_time = -1
# function receiveing the new joint positions
def callback(data):
    global joint_positions
    global data_time
    joint_name_list = chosenSolver.joint_name_list
    data_time = data.header.stamp

    all_joint_name_received = data.name

    for i in range(len(joint_name_list)):
        name_cons = joint_name_list[i]
        if (name_cons in all_joint_name_received):
            value = data.position[all_joint_name_received.index(name_cons)]
            joint_positions[i] = value

def plot_polytope(q, direction = None):
    # calculate dk of the robot
    name = namespace
    if (namespace[0] == "/"):
        name = name[1:]

    pose = chosenSolver.dk_position(q)

    # calculate force vertexes
    start = time.time()
    if direction is not None: 
        force_vertex, force_polytopes = chosenSolver.force_polytope(q, direction)  
    else:
        force_vertex, force_polytopes = chosenSolver.force_polytope(q)


    end = time.time()
    #print("---")
    print("Time for force vertexes : ", end - start)

    # construct vertex pointcloud
    pointcloud_massage = PointCloud()
    if direction is not None: 
        force_vertex = (direction).T * force_vertex
    for i in range(force_vertex.shape[1]):
        point = Point32()
        point.x = force_vertex[0,i]/500 + pose[0]
        point.y = force_vertex[1,i]/500 + pose[1]
        point.z = force_vertex[2,i]/500 + pose[2]
        pointcloud_massage.points.append(point)

    # polytop stamped message
    pointcloud_massage.header = Header()
    pointcloud_massage.header.frame_id = name+chosenSolver.base_link
    pointcloud_massage.header.stamp = rospy.Time.now() #rospy.Time.now()
    # publish plytop
    publish_force_polytop_vertex = rospy.Publisher(namespace+'panda/force_polytope_vertex', PointCloud, queue_size=10)
    publish_force_polytop_vertex.publish(pointcloud_massage)


    # draw polytope in only for 2D and 3D
    if force_polytopes[0].shape[0] > 1:
        polygonarray_message = PolygonArray()
        polygonarray_message.header = Header()
        polygonarray_message.header.frame_id = name+chosenSolver.base_link
        polygonarray_message.header.stamp = rospy.Time.now() #rospy.Time.now()
        for face_polygon in force_polytopes:
            polygon_massage = Polygon()
            if direction is not None: 
                face_polygon = (direction).T * face_polygon
            for i in range(face_polygon.shape[1]):
                point = Point32()
                point.x = face_polygon[0,i]/500 + pose[0]
                point.y = face_polygon[1,i]/500 + pose[1]
                point.z = face_polygon[2,i]/500 + pose[2] 
                polygon_massage.points.append(point)

            # polytope stamped message
            polygon_stamped = PolygonStamped()
            polygon_stamped.polygon = polygon_massage
            polygon_stamped.header = Header()
            polygon_stamped.header.frame_id = name+chosenSolver.base_link
            polygon_stamped.header.stamp = rospy.Time.now() #rospy.Time.now()
            polygonarray_message.polygons.append(polygon_stamped)
            polygonarray_message.likelihood.append(1.0)
            

        # publish plytop
        publish_force_polytope = rospy.Publisher(namespace+'panda/force_polytope', PolygonArray, queue_size=10)
        publish_force_polytope.publish(polygonarray_message)

    f_v,f_p = chosenSolver.force_polytope(q,[0,0,1])

    fmax = np.max(f_v)
    
    f_max_msg = Float64(fmax/9.81)
    publish_fmax = rospy.Publisher(namespace+'panda/max_force_z', Float64, queue_size=2)
    publish_fmax.publish(f_max_msg)
    

# main function of the class
def panda_polytope():
    global joint_positions
    rospy.init_node('panda_polytope')

    joint_state_topic = rospy.get_param('~joint_state_topic', 'Q_pub')

    rospy.Subscriber(namespace+joint_state_topic, JointState, callback, queue_size= 2)

    polytope_direction = None 
    #polytope_direction = np.array([[1,0,0],[0 ,0 ,1]]) 

    rate = rospy.Rate(100) #Hz
    while not rospy.is_shutdown():
        plot_polytope(joint_positions, polytope_direction)
        rate.sleep()

# class definition
if __name__ == '__main__':
    try:
        panda_polytope()
    except rospy.ROSInterruptException:
        pass