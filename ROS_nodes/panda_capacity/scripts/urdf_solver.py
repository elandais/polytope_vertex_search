#!/usr/bin/env python3

# to assure compatiblity with Python3
# (and assuming Python3 packages are installed into 
# the path designed by the variable "PYTHON3_LIB_PATH")
import sys
import os
version = float(sys.version.split(" ")[0][0:3])
if version >= 2.7:
    try : 
        python3_path = os.environ["PYTHON3_LIB_PATH"].split(":")
        sys.path = python3_path + sys.path
    except KeyError:
        pass

from math import sin, cos 
import numpy as np
import numpy.matlib 
import itertools

# minkowski sum
from scipy.spatial import ConvexHull, Delaunay
#from panda_capacity.scripts.panda_solver import T_min_int

from urdf_parser_py.urdf import URDF
from pykdl_utils.kdl_kinematics import KDLKinematics
import hrl_geom.transformations as trans

import pinocchio as pin
from pinocchio.robot_wrapper import RobotWrapper


class URDFSolver(object):

    def __init__(self):
        self.t_max = -1
        self.t_min = -self.t_max
        # maximal joint velocities
        self.dq_max = np.zeros((1,1))
        self.dq_min = -self.dq_max
        # maximal joint angles
        self.q_max = np.zeros((1,1))
        self.q_min = np.zeros((1,1))
        # matrix of axis vectors - for polytope search
        self.T_vec = np.zeros((1,1))
        # matrix of 
        self.T_min = np.zeros((1,1))
        self.n = -1
        self.m = 3

    def dk_position(self,q):
        return self.forward(q)[:3,3]

    def dk_orientation_matrix(self,q):
        return self.forward(q)[0:3,0:3]

    def jacobian_position(self,q):
        return self.jacobian(q)[:3, :]

    def jacobian_pseudo_inv(self,q):
        return np.linalg.pinv(self.jacobian(q))

    def forward(self,q):
        pass

    def jacobian(self,q):
        pass
    # iterative solving of inverse kinematics
    def ik_solve(x_d, q_0, iterations):
        pass

    def gravity_torque(self,q):
        pass

    # velocity manipulability calculation
    def manipulability_velocity(self,q, direction = None):
        # avoid 0 angles
        joint_pos = np.array(q)
        joint_pos[joint_pos==0] = 10**-7

        # jacobian calculation
        if direction is None:
            Jac = self.jacobian_position(q)
        else:
            Jac = np.array(direction).dot(self.jacobian_position(q))
        # limits scaling
        W = np.diagflat(self.dq_max)

        Mat = np.dot(Jac,W)

        # calculate the singular value decomposition
        U, S, V = np.linalg.svd(Mat)
        # return the singular values and the unit vector angle
        return [S, U]

    # force manipulability calculation
    def manipulability_force(self,q, direction = None):
        # avoid 0 angles
        joint_pos = np.array(q)
        joint_pos[joint_pos==0] = 10**-7

        # jacobian calculation
        if direction is None:
            Jac = self.jacobian_position(q)
        else:
            Jac = np.array(direction).dot(self.jacobian_position(q))
        # limits scaling
        W = np.linalg.pinv(np.diagflat(self.t_max))
        # calculate the singular value decomposition

        Mat = np.dot(Jac,W)
        U, S, V = np.linalg.svd(Mat)

        # return the singular values and the unit vector angle
        return [np.divide(1,S), U]

    # maximal force in direction
    def max_force_in_direction(self,q, dir):
        n = self.n
        t_max = self.t_max
        T_vec = self.T_vec
        # jacobian calculation
        Jac = np.array(dir).transpose()*self.jacobian_position(q)
        m = 1
        # calculate svd
        U, S, V = np.linalg.svd(Jac)
        r = np.linalg.matrix_rank(Jac)
        V1 = np.array(V.transpose()[:,:m])
        V2 = np.array(V.transpose()[:,m:])

        J_n_invT = np.linalg.pinv(Jac.transpose())

        f_vertex = []
        t_vertex = []

        # A loop to go through all pairs of adjacent vertices
        for i in range(self.n):
            # find all n-m face vector indexes
            face_vectors = np.delete(range(n), [i]) 
            S = self.T_min[:,0:2].copy()
            S[i,1] = t_max[i]
            S_v2 = V2.transpose().dot(S)

            # vectors to be used
            T = T_vec[:,face_vectors]
            # solve the linear system for the edge vectors tl and tj
            Z = V2.transpose().dot(-T)

            # figure out if some solutions can be discarded
            Z_min = Z.copy()
            Z_min[Z_min > 0] = 0
            Z_max = Z.copy()
            Z_max[Z_max < 0] = 0
            S_min = Z_min.dot(np.ones((n-m,1)))
            S_max = Z_max.dot(np.ones((n-m,1)))
            to_reject = np.any(S_v2 - S_min < - 10**-7, axis=0) + np.any(S_v2 - S_max > 10**-7, axis=0)
            if np.all(to_reject): # all should be discarded
                continue
            S = S[:,~to_reject]
            S_v2 = V2.transpose().dot(S)
            
            # check the rank and invert
            Z_inv = np.linalg.pinv(Z)
                                    
            # calculate the forces for each face
            X = Z_inv.dot(S_v2)
            # check if inverse correct - all error 0
            t_err = np.any( abs(S_v2 - Z.dot(X)) > 10**-7, axis=0) 
            # remove the solutions that are not in polytope 
            to_remove = (np.any(X < -10**-7, axis=0) + np.any(X - 1 > 10**-7 , axis=0)) + t_err
            X= X[:, ~to_remove]
            S= S[:, ~to_remove]
            if t_vertex == []:
                t_vertex =  S+T.dot(X)
            # add vertex torque
            t_vertex = np.hstack((t_vertex, S+T.dot(X)))

        # calculate the forces based on the vertex torques
        f_vertex = np.dot(J_n_invT,( t_vertex- self.gravity_torque(q) ) )
        return np.max(f_vertex)


    # maximal end effector force
    def force_polytope_intersection_auctus(self,q1,q2):
        T_min_int = self.T_min_int
        t_max_int = self.t_max_int
        T_vec_int = self.T_vec_int

        # jacobian calculation
        Jac1 = self.jacobian_position(q1)
        Jac2 = self.jacobian_position(q2)
        Jac =  np.hstack((Jac1,Jac2))
        
        m, n = Jac.shape

        # calculate svd
        U, S, V = np.linalg.svd(Jac)
        r = np.linalg.matrix_rank(Jac)
        V1 = np.array(V.transpose()[:,:m])
        V2 = np.array(V.transpose()[:,m:])

        J_n_invT = np.linalg.pinv(Jac.transpose())
        gravity = np.vstack((self.gravity_torque(q1), self.gravity_torque(q2)))
        T_min_g = T_min_int - gravity
        t_max_g = t_max_int - gravity

        f_vertex = []
        t_vertex = []
        # A loop to go through all pairs of adjacent vertices
        for i in range(n):
            for j in range(i+1, n):
                for k in range(j+1, n):
                    # find all n-m face vector indexes
                    face_vectors = np.delete(range(n), [i, j, k]) 
                    S = T_min_g.copy()
                    S[i,[0,1,2,3]] = t_max_g[i]
                    S[j,[0,1,4,5]] = t_max_g[j]
                    S[k,[0,2,4,6]] = t_max_g[k]
                    S_v2 = V2.transpose().dot(S)

                    # vectors to be used
                    T = T_vec_int[:,face_vectors]
                    # solve the linear system for the edge vectors tl and tj
                    Z = V2.transpose().dot(-T)

                    # figure out if some solutions can be discarded
                    Z_min = Z.copy()
                    Z_min[Z_min > 0] = 0
                    Z_max = Z.copy()
                    Z_max[Z_max < 0] = 0
                    S_min = Z_min.dot(np.ones((n-m,1)))
                    S_max = Z_max.dot(np.ones((n-m,1)))
                    to_reject = np.any(S_v2 - S_min < - 10**-7, axis=0) + np.any(S_v2 - S_max > 10**-7, axis=0)
                    if np.all(to_reject): # all should be discarded
                        continue
                    S = S[:,~to_reject]
                    S_v2 = V2.transpose().dot(S)
                    # check the rank and invert
                    Z_inv = np.linalg.pinv(Z)
                                            
                    # calculate the forces for each face
                    X = Z_inv.dot(S_v2)
                    # check if inverse correct - all error 0
                    t_err = np.any( abs(S_v2 - Z.dot(X)) > 10**-7, axis=0) 
                    # remove the solutions that are not in polytope 
                    to_remove = (np.any(X < -10**-7, axis=0) + np.any(X - 1 > 10**-7 , axis=0)) + t_err
                    X= X[:, ~to_remove]
                    S= S[:, ~to_remove]
                    if t_vertex == []:
                        t_vertex =  S+T.dot(X)
                    # add vertex torque
                    t_vertex = np.hstack((t_vertex, S+T.dot(X)))

        t_vertex = self.make_unique(t_vertex)
        # calculate the forces based on the vertex torques
        f_vertex = np.dot(J_n_invT,( t_vertex ) )

        return f_vertex, t_vertex, gravity


    # maximal end effector force
    def force_polytope_sum_auctus(self,q1,q2):

        m = self.m

        f_vertex1, t_vertex1, gravity1 = self.force_polytope_auctus(q1)
        f_vertex2, t_vertex2, gravity2 = self.force_polytope_auctus(q2)
        
        f_sum = np.zeros((f_vertex1.shape[1]*f_vertex2.shape[1],m))
        for i in range(f_vertex1.shape[1]):
            for j in range(f_vertex2.shape[1]):
                f_sum[i*f_vertex2.shape[1] + j] = np.array(f_vertex1[:,i]+f_vertex2[:,j]).flat

        hull = ConvexHull(f_sum, qhull_options='QJ')
        f_vertex = np.array(f_sum[hull.vertices]).T

        polytope = []
        for face in hull.simplices:
            polytope.append(np.array(f_sum[face]).T)

        return f_vertex, polytope


    # maximal end effector force
    def force_polytope_auctus(self,q, direction = None, force = None):
        t_min = self.t_min
        t_max = self.t_max

        # jacobian calculation
        Jac_full = self.jacobian_position(q)
        if direction is not None:
            Jac = np.dot( np.array([direction]), Jac_full )
        
        else:
            Jac = Jac_full

        m, n = Jac.shape

        # calculate svd
        U, S, V = np.linalg.svd(Jac)
        r = np.linalg.matrix_rank(Jac)
        V1 = np.array(V.transpose()[:,:m])
        V2 = np.array(V.transpose()[:,m:])

        # jacobian matrix pseudo inverse
        J_n_invT = np.linalg.pinv(Jac.transpose())

        # matrix of axis vectors - for polytope search
        T_vec = np.diagflat(t_max-t_min)

        if force is None:
            # gravity = np.zeros((n,1))
            gravity = self.gravity_torque(q)
        else:
            tn = np.asarray(Jac_full.T.dot(force).T)
            gravity = self.gravity_torque(q) + tn
            #gravity = np.zeros((n,1)) + tn.T

        T_min = np.matlib.repmat(t_min - gravity,1,2**m)
        t_max_g = t_max - gravity

        fixed_vectors_combinations = np.array(list(itertools.combinations(range(n),m)))
        permutations = np.array(list(itertools.product([0, 1], repeat=m))).T
        
        f_vertex = []
        t_vertex = []
        # A loop to go through all pairs of adjacent vertices
        for fixed_vectors in fixed_vectors_combinations:
            # find all n-m face vector indexes
            face_vectors = np.delete(range(n), fixed_vectors) 

            S = T_min.copy()
            for i in range(m): S[fixed_vectors[i], permutations[i] > 0] = t_max_g[fixed_vectors[i]]
            S_v2 = V2.transpose().dot(S)

            # vectors to be used
            T = T_vec[:,face_vectors]
            # solve the linear system for the edge vectors tl and tj
            Z = V2.transpose().dot(-T)

            # figure out if some solutions can be discarded
            Z_min = Z.copy()
            Z_min[Z_min > 0] = 0
            Z_max = Z.copy()
            Z_max[Z_max < 0] = 0
            S_min = Z_min.dot(np.ones((n-m,1)))
            S_max = Z_max.dot(np.ones((n-m,1)))

            to_reject = np.any(S_v2 - S_min < - 10**-7, axis=0) + np.any(S_v2 - S_max > 10**-7, axis=0)

            if np.all(to_reject): # all should be discarded
                continue

            S = S[:,~to_reject]
            S_v2 = V2.transpose().dot(S)
            
            # check the rank and invert
            Z_inv = np.linalg.pinv(Z)
                                    
            # calculate the forces for each face
            X = Z_inv.dot(S_v2)
            # check if inverse correct - all error 0
            t_err = np.any( abs(S_v2 - Z.dot(X)) > 10**-7, axis=0) 
            # remove the solutions that are not in polytope 
            to_remove = (np.any(X < -10**-7, axis=0) + np.any(X - 1 > 10**-7 , axis=0)) + t_err
            X= X[:, ~to_remove]
            S= S[:, ~to_remove]

            if t_vertex == []:
                t_vertex =  S+T.dot(X)
            # add vertex torque
            t_vertex = np.hstack((t_vertex, S+T.dot(X)))


        t_vertex = self.make_unique(t_vertex)
        # calculate the forces based on the vertex torques
        f_vertex = np.dot(J_n_invT,( t_vertex ) )
        return f_vertex, t_vertex, gravity

    # returns the force polytope vertices 
    # and the list of faces with ordered vertices
    def force_polytope_ordered(self,q, direction = None, force = None):
        t_min_int = self.t_min_int
        t_max_int = self.t_max_int
        n = self.n

        force_vertex, t_vertex, gravity = self.force_polytope_auctus(q, direction, force)
        polytopes = []
        if force_vertex.shape[0] == 1:
            polytopes.append(force_vertex)
        elif force_vertex.shape[0] == 2:
            polytopes.append(force_vertex[:, self.order_index(force_vertex)])
        else:        
            for i in range(n):
                fi = np.array(force_vertex[:,np.isclose(t_vertex[i,:], (t_min_int[i] - gravity[i]),1e-5)])
                if fi != []:
                    if fi.shape[1] > 3:
                        polytopes.append(fi[:,self.order_index(self.make_2d(fi))])
                    else: 
                        polytopes.append(fi)
                fi = np.array(force_vertex[:,np.isclose(t_vertex[i,:], (t_max_int[i] - gravity[i]),1e-5)])
                if fi != []:
                    if fi.shape[1] > 3:
                        polytopes.append(fi[:,self.order_index(self.make_2d(fi))])
                    else: 
                        polytopes.append(fi)
        return [force_vertex, polytopes]

    # returns the vertices of intersection of force polytopes  
    # and the list of faces with ordered vertices
    def force_polytope_intersection_ordered(self,q1,q2):
        n = self.n
        t_min_int = self.t_min_int
        t_max_int = self.t_max_int

        force_vertex, t_vertex, gravity = self.force_polytope_intersection_auctus(q1,q2)
        polytopes = []
        for i in range(2*n):
            fi = np.array(force_vertex[:,np.isclose(t_vertex[i,:], (t_min_int[i] - gravity[i]),1e-5)])
            if fi != []:
                if fi.shape[1] > 3:
                    polytopes.append(fi[:,self.order_index(self.make_2d(fi))])
                else: 
                    polytopes.append(fi)
            fi = np.array(force_vertex[:,np.isclose(t_vertex[i,:], (t_max_int[i] - gravity[i]),1e-5)])
            if fi != []:
                if fi.shape[1] > 3:
                    polytopes.append(fi[:,self.order_index(self.make_2d(fi))])
                else: 
                    polytopes.append(fi)
        return [force_vertex, polytopes]

    # take coplanar points and project them in 2d
    def make_2d(self,points):
        U = points[:,1]-points[:,0]   # define 1st ortogonal vector
        for i in range(2, points.shape[1]): # find 2nd ortogonal vector (avoid collinear)
            V = points[:,i]-points[:,0]
            if abs(abs(U.dot(V)) - np.linalg.norm(U)*np.linalg.norm(V)) > 10**-7:
                break
        
        U = U / np.linalg.norm(U)
        V = V / np.linalg.norm(V)

        W = np.cross(U,V) # this will be near zero if A,B,C are on single line
        U = np.cross(V,W)
        
        x = U.dot(points)
        y = V.dot(points)

        return np.array([x, y])
    # order the points 
    def order_index(self,points):
        px = np.array(points[0,:]).ravel()
        py = np.array(points[1,:]).ravel()
        p_mean = np.array(np.mean(points,axis=1)).ravel()

        angles = np.arctan2( (py-p_mean[1]), (px-p_mean[0]))
        sort_index = np.argsort(angles)
        return sort_index
    # remove repeating solutions
    def make_unique(self,points):
        #print("Points : ",points)
        unique_points = np.unique(np.around(points,7), axis=1)
        return unique_points


class URDFSolverKDL(URDFSolver):
    def __init__(self, description_string, base_link, end_link):
        super(URDFSolverKDL,self).__init__()
        # Physical parameters 
        # URDF file can be parsed instead
        self.robot = URDF.from_parameter_server(description_string)
        self.base_link = base_link
        self.end_link = end_link

        self.kdl_kin = KDLKinematics(self.robot, base_link, end_link)

        self.joint_name_list = self.kdl_kin.get_joint_names()

        # dof, output space
        self.n = len(self.kdl_kin.joint_limits_lower)
        # output space
        self.m = 3

        # https://frankaemika.github.io/docs/control_parameters.html
        # maximal torques
        self.t_max =  np.array([self.kdl_kin.joint_limits_effort]).T #np.array([[87], [87], [87], [87], [12], [12], [12]]) #np.array([[1], [1], [1], [1], [1], [1], [1]])
        self.t_min = -self.t_max
        # maximal joint velocities
        self.dq_max = np.array([self.kdl_kin.joint_limits_velocity]).T
        self.dq_min = -self.dq_max
        # maximal joint angles
        self.q_max = np.array([self.kdl_kin.joint_limits_upper]).T
        self.q_min = np.array([self.kdl_kin.joint_limits_lower]).T
        # matrix of axis vectors - for polytope search
        self.T_vec = np.diagflat(self.t_max-self.t_min)
        # matrix of 
        self.T_min = np.matlib.repmat(self.t_min,1,2**(self.m) )

        # intersection
        self.t_max_int =  np.vstack((self.t_max,self.t_max))
        self.t_min_int = -self.t_max_int
        self.T_vec_int = np.diagflat((self.t_max-self.t_min,self.t_max-self.t_min))
        self.T_min_int = np.matlib.repmat(self.t_min_int,1,2**(self.m) )

    # Transformation matrix of end_link
    def forward(self,q):
        # print(self.joint_name_list)
        return self.kdl_kin.forward(q)

    def jacobian(self,q):
        Jac = self.kdl_kin.jacobian(q)
        #print("kdl :", Jac)
        return Jac

    # iterative solving of inverse kinematics
    def ik_solve(self,x_d, q_0, iterations):
        return  self.kdl_kin.inverse(x_d, q_0)

    def gravity_torque(self,q):
        return np.array([self.kdl_kin.gravity(q)]).T

class URDFSolverPIN(URDFSolver):
    
    def __init__(self, urdf_filename, base_link, end_link):
        """!
        NB : main difference compared to URDFSolverKDL is that
        the class needs the absolute path to the .urdf file, instead
        of the name of the parameter describing the urdf.
        """
        super(URDFSolverPIN,self).__init__()

        self.model = pin.buildModelFromUrdf(urdf_filename)
        self.data = self.model.createData()

        self.base_link = base_link
        self.end_link = end_link

        self.id_base = self.model.getBodyId(self.base_link)

        self.joint_base_id = self.model.frames[self.id_base].parent

        self.id_end = self.model.getBodyId(self.end_link)

        self.joint_end_id = self.model.frames[self.id_end].parent

        # joints between the base_link and the end_link
        joint_ids_base = self.model.supports[self.joint_base_id]
        joint_ids_end = self.model.supports[self.joint_end_id]

        self.joint_ids = joint_ids_end

        size = len(self.joint_ids)
        k = 0
        while k < size:
            if (self.joint_ids[k] in joint_ids_base):
                del(self.joint_ids[k])
                k-=1
                size -= 1
            k+=1

        self.joints_q_id = []

        self.body_name_list = []

        self.joint_name_list = []

        self.n = len(self.joint_ids)
        # output space
        self.m = 3

        self.t_max =  np.zeros((self.n, 1)) #np.array([[87], [87], [87], [87], [12], [12], [12]]) #np.array([[1], [1], [1], [1], [1], [1], [1]])
        # maximal joint velocities
        self.dq_max = np.zeros((self.n, 1))
        # maximal joint angles
        self.q_max = np.zeros((self.n, 1))
        self.q_min = np.zeros((self.n, 1))

        self.q = pin.neutral(self.model)

        # simple test, not useful
        for k in range(len(self.model.frames)):
            jid = self.model.frames[k].parent
            name = self.model.frames[k].name
            if (jid in self.joint_ids and name not in self.joint_name_list):
                self.body_name_list.append(self.model.frames[k].name)
        
        # NB : the order of the joints into self.model.joints and self.q
        # are not the same.
        # Ex : the 2nd joint designed by self.model.joints[1] may not be the joint
        # designed by self.q[1].
        # This is why two objects are used here : 
        # * in order to receive the joint values, we use self.joints_ids, linked to
        # the order of self.model.joints.
        # * in order to compute all necessary operations, we use self.joints_q_id, linked
        # to the order of self.q.

        # first, in order to get all the necessary parameters for the
        # reception of data
        for i in range(len(self.joint_ids)):
            jid = self.joint_ids[i]

            joint = self.model.joints[jid]
            nq = joint.nq
            idx_q = joint.idx_q
            self.joints_q_id.append([idx_q,idx_q+nq])
            self.joint_name_list.append(self.model.names[jid] )            
        
        # put all useful parameters for computation (we suppose the urdf
        # only has joints with 1 DOF).        
        for i in range(len( self.joints_q_id)):

            q_ind = self.joints_q_id[i][0]
            self.t_max[i,0] = self.model.effortLimit[q_ind]
            self.dq_max[i,0] = self.model.velocityLimit[q_ind]
            self.q_max[i,0] = self.model.upperPositionLimit[q_ind]
            self.q_min[i,0] = self.model.lowerPositionLimit[q_ind]

        self.t_min = -self.t_max
        self.dq_min = -self.dq_max

        # dof, output space

        # https://gepettoweb.laas.fr/doc/stack-of-tasks/pinocchio/master/doxygen-html/structpinocchio_1_1ModelTpl.html
        # maximal torques

        # matrix of axis vectors - for polytope search
        self.T_vec = np.diagflat(self.t_max-self.t_min)
        # matrix of 
        self.T_min = np.matlib.repmat(self.t_min,1,2**(self.m) )

        # intersection
        self.t_max_int =  np.vstack((self.t_max,self.t_max))
        self.t_min_int = -self.t_max_int
        self.T_vec_int = np.diagflat((self.t_max-self.t_min,self.t_max-self.t_min))
        self.T_min_int = np.matlib.repmat(self.t_min_int,1,2**(self.m) )

    # Transformation matrix of end_link
    def forward(self,q):

        for i in range(len(self.joints_q_id)):
            nq1,nq2 = self.joints_q_id[i][0], self.joints_q_id[i][1]
            self.q[nq1] = q[i]
        
        pin.forwardKinematics(self.model,self.data,self.q)
        pin.updateFramePlacement(self.model, self.data, self.id_end)
        pin.updateFramePlacement(self.model, self.data, self.id_base)

        base_frame =  self.data.oMf[self.id_base].np

        end_frame =  self.data.oMf[self.id_end].np

        Fp = np.dot( np.linalg.inv(base_frame), end_frame)

        return( Fp )

    def jacobian(self,q):
        """!

        Particularity here : the Jacobian should be expressed on the origin of the referential
        linked to the end_effector, with the same orientation than the axes of the referential of
        the base_link.
        (NB : no translation term is necessary, even if it seems weird). 

        Pbme : best that Pinocchio can do for this is to return the Jacobian of the origin of the end_effector,
        expressed into the referential of the end_effector.
        Solution : apply to the Jacobian the rotation term of the transformation matrix from the base_link
        to the end_link.

        """

        for i in range(len(self.joints_q_id)):
            nq1,nq2 = self.joints_q_id[i][0], self.joints_q_id[i][1]
            self.q[nq1] = q[i]

        Jac = pin.computeFrameJacobian(self.model,self.data,self.q,self.id_end,pin.LOCAL)

        Jac_specific = np.zeros((6,self.n))
        for i in range(len(self.joints_q_id)):
            col = self.joints_q_id[i][0]
            Jac_specific[:,i] = Jac[:,col]
        
        transfo_rot = self.forward(q)[:3,:3]
        Jac_specific[:3,:] = np.dot(transfo_rot, Jac_specific[:3,:])
        Jac_specific[3:,:] = np.dot(transfo_rot, Jac_specific[3:,:])

        return Jac_specific

    # iterative solving of inverse kinematics
    def ik_solve(self,x_d, q_0, iterations):

        for i in range(len(self.joints_q_id)):
            nq1,nq2 = self.joints_q_id[i][0], self.joints_q_id[i][1]
            self.q[nq1] = q_0[i]

        pos = x_d[0:3]
        rot = x_d[3:6]
        Lframe_id = [self.id_end]
        
        rot_mat =  pin.utils.rotate('x',rot[0]) * pin.utils.rotate('y',rot[1]) * pin.utils.rotate('z',rot[2]) 
        pos_mat = np.array([pos[0],pos[1],pos[2]])
        Lcons = [[rot_mat,pos_mat]]

        q, self.data, err = moveListFrame(Lframe_id, Lcons, self.model, self.data, self.q, iterations )
        return(q,err)


    def gravity_torque(self,q):
        for i in range(len(self.joints_q_id)):
            nq1,nq2 = self.joints_q_id[i][0], self.joints_q_id[i][1]
            self.q[nq1] = q[i]

        Grav = np.array([pin.computeGeneralizedGravity(self.model, self.data,self.q)]).T

        Grav_specific = np.zeros((self.n,1))

        for i in range(len(self.joints_q_id)):
            line = self.joints_q_id[i][0]
            Grav_specific[i,0] = Grav[line,0]

        return Grav_specific


def moveListFrame(Lframe_id,Lcons,urdf_model,urdf_data, q0, iterations,verbose=False):
    """!

    Given a list of frames name Lname, a list of constraints to be reached 
    Lcons, a Pinocchio model urdf_model and its associated data object urdf_data, execute
    an Inverse Kinematic operation in order to determine the configuration q to 
    reach the constraints, if these constraints can be achieved. 
    A message is displayed if those constraints are achieved or not. 
    Inspired from https://gepettoweb.laas.fr/doc/stack-of-tasks/pinocchio/master/doxygen-html/md_doc_b-examples_i-inverse-kinematics.html .
    
    @param Lframe_id : List.
    
        List of the id of the frames on which the Inverse Kinematics operation is to be applied.
    
    @param Lcons : List of list.
    
        List of the constraint to be reached by the selected joints (expressed in ground referential).
        One element is organized as follows : 
        [ Rotation matrix, Position matrix ]
        Example : 
        [  pin.utils.rotate('x',3.0)  , np.array([0.08, -0.07, 0.86]) ]
    
    @param urdf_model : Pinocchio model object.
        Pinocchio object containing all the physical description of the loaded URDF model, 
        including kinematic and inertial parameters defining its structure.
        Please check https://gepettoweb.laas.fr/doc/stack-of-tasks/pinocchio/master/doxygen-html/md_doc_a-features_intro.html 
        for more informations.
        
    @param urdf_data : Pinocchio data object.
         Pinocchio object containing all the values of the loaded URDF model which
         are the result of a computation. (joint configuration, velocity, ...).

    @param verbose : Boolean.
        Activate the print functions of the algorithm. Useful for debugging.
        The default is False.

    @return q : numpy.array
        Vector Q from the Inverse Kinematics operation.
    
    @return urdf_data : Pinocchio data object.
         The pinocchio data object updated so the joints have the values set in 
         the Q vector.
    
    @return err : numpy.array (6 x number_of_joints, )
        Final error vector between current and desired joints values.

    """
    L_oMdes = []
    
     
    for k in range(len(Lcons)):
        try:
            oMdes = pin.SE3( Lcons[k][0] , Lcons[k][1])
        except TypeError:
            oMdes = Lcons[k]
        L_oMdes.append(oMdes) 

    q      = q0

    # minimum difference between current and desired joints values to stop the
    # Inverse Kinematics operation.
    eps    = 1e-4
    # the maximum number of iterations of the Inverse Kinematics operation.
    IT_MAX = iterations
    # time step
    DT     = 1e-1
    # regularization value
    damp   = 1e-6
    #print(L_oMdes[0].toActionMatrix() )
    # print( L_oMdes[0].actInv(urdf_data.oMi[Lframe_id[0]] ) )

    # M1 = L_oMdes[0]
    # M2 = urdf_data.oMi[Lframe_id[0]]

    # R3 = np.dot(M1.rotation.T,M2.rotation)

    # print("R3 : ", R3)

    i=0
    while True:
          nb_cons = len(Lframe_id)
          # forward kinematics to the urdf model
          pin.forwardKinematics(urdf_model,urdf_data,q)
          
          # check the difference between current and desired joints values.
          err = np.zeros((1,1))
          for k in range(len(L_oMdes)):
            dMi= L_oMdes[k].actInv(urdf_data.oMf[Lframe_id[k]] )
            #print(dMi)
            if err.shape == (1,1):
                err = pin.log(dMi).vector
            else:
                err = np.hstack( ( err,  pin.log(dMi).vector ) )
         
          if np.norm(err) < eps:
              success = True
              break
          if i >= IT_MAX:
              success = False
              break
    
          # get the Jacobian of each of the joints concerned by the Inverse Kinematics
          # operation.
          J = np.zeros((1,1))
          for k in range(len(Lframe_id)):
              Jk = pin.computeFrameJacobian(urdf_model,urdf_data,q,Lframe_id[k])
              if J.shape == (1,1):
                  J = Jk
              else:
                  J = np.vstack( (J, Jk) )
         
          # get the velocity vector.
          v = - J.T.dot( np.solve(J.dot(J.T) + damp * np.eye(6*nb_cons), err)           )
          
          # get the vector Q according to the velocity vector.
          q = pin.integrate(urdf_model,q,v*DT)
              
          if not i % 10 and verbose:
              print('%d: error = %s' % (i, err.T))
          i += 1
    
    if verbose:
        if success :
            print("Convergence achieved!")
        else:
            print("\nWarning: the iterative algorithm has not reached convergence to the desired precision")
                
        print('\nresult: %s' % q.flatten().tolist())
        print('\nfinal error: %s' % err.T)

    return(q,urdf_data,err)

def printConfig(urdf_model,urdf_data):
    """!
    
    Displays the position and orientation of each of the joints of the .urdf
    model, expressed into the global referential associated with the .urdf. 
    
    @param urdf_model : Pinocchio model object.
        Pinocchio object containing all the physical description of the loaded URDF model, 
        including kinematic and inertial parameters defining its structure.
        Please check https://gepettoweb.laas.fr/doc/stack-of-tasks/pinocchio/master/doxygen-html/md_doc_a-features_intro.html 
        for more informations.
        
    @param urdf_data : Pinocchio data object.
         Pinocchio object containing all the values of the loaded URDF model which
         are the result of a computation. (joint configuration, velocity, ...).

    @return None.

    """    
    print("----- Translation -----")    
    for name, oMi in zip(urdf_model.names, urdf_data.oMi):
        print(("{:<24} : {: .4f} {: .4f} {: .4f}"
              .format( name, *oMi.translation.T.flat )))
    print("----- Rotation -----")
    for name, oMi in zip(urdf_model.names, urdf_data.oMi):
        print(("{:<24} : {: .4f} {: .4f} {: .4f} "
            .format(name, *list(trans.euler_from_rot_mat(oMi.rotation)))) )

def printQ(urdf_model,q):
    """!
    
    Displays the value of each of the articulations of the .urdf model.

    @param urdf_model : Pinocchio model object.
        Pinocchio object containing all the physical description of the loaded URDF model, 
        including kinematic and inertial parameters defining its structure.
        Please check https://gepettoweb.laas.fr/doc/stack-of-tasks/pinocchio/master/doxygen-html/md_doc_a-features_intro.html 
        for more informations.
        
    @param q : numpy.array
        Vector Q associated with the .urdf model. 

    @return None.

    """
    print("----- Q from URDF -----")    
    for joint,name in zip(urdf_model.joints,urdf_model.names):
        nq = joint.nq
        idx_q = joint.idx_q
        q_joint = q[idx_q:idx_q+nq]
        print(" %s : "%name, q_joint)



# definition of the four_link_solver module
if __name__ == '__main__':
    URDFSolverKDL("/human_description","torso","end_effector_l") 