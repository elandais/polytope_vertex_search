#!/usr/bin/env python3
import rospy

import numpy as np
from visualization_msgs.msg import Marker
from sensor_msgs.msg import JointState
# robot module import
import urdf_solver as panda
import hrl_geom.transformations as trans

import capacity.robot_solver as capRob

base_link = "torso"
end_link = "end_effector_l"

# chosenSolver = capRob.URDFSolverKDL("/human_description",base_link,end_link)

urdf_path = "/home/elandais/catkin_ws/src/human_control/description/two_arms_only_marks/two_arms_only_marks_aCol.urdf.xacro"
chosenSolver = capRob.URDFSolverPIN(urdf_path,base_link,end_link)

#kdlSolver = panda.URDFSolverKDL("/human_description",base_link,end_link)

namespace = rospy.get_namespace()

joint_positions = [0 for k in range( len(chosenSolver.joint_name_list) )]

data_time = -1

# function receiveing the new joint positions
def callback(data):
    global joint_positions
    global data_time
    joint_name_list = chosenSolver.joint_name_list
    data_time = data.header.stamp

    all_joint_name_received = data.name

    for i in range(len(joint_name_list)):
        name_cons = joint_name_list[i]
        if (name_cons in all_joint_name_received):
            value = data.position[all_joint_name_received.index(name_cons)]
            joint_positions[i] = value

# function drawing the marker upon receiving new joint positions
def draw_ellipsoids(q):
    
    # calculate dk of the robot
    pose = chosenSolver.dk_position(q)


    manipulability_v = chosenSolver.manipulability_velocity(q)
    Rot_v = np.identity(4)
    Rot_v[0:3,0:3] = manipulability_v[1]
    S = manipulability_v[0]
    name = namespace
    if (namespace[0] == "/"):
        name = name[1:]
    marker = Marker()
    marker.header.frame_id = name+chosenSolver.base_link
    marker.header.stamp = rospy.Time.now()
    marker.pose.position.x = pose[0]
    marker.pose.position.y = pose[1]
    marker.pose.position.z = pose[2]
    quaternion =  trans.quaternion_from_matrix( Rot_v)
    marker.pose.orientation.x = quaternion[0]
    marker.pose.orientation.y = quaternion[1]
    marker.pose.orientation.z = quaternion[2]
    marker.pose.orientation.w = quaternion[3]

    marker.type = marker.SPHERE
    marker.color.g = 1.0
    marker.color.r = 0.7
    marker.color.a = 0.5
    marker.scale.x = 2*S[0]/5
    marker.scale.y = 2*S[1]/5
    marker.scale.z = 2*S[2]/5

    publish_manip1 = rospy.Publisher(namespace+'panda/manip_velocity', Marker, queue_size=10)
    publish_manip1.publish(marker)

    # calculate manipulability
    manipulability_f = chosenSolver.manipulability_force(q)

    Rot_f = np.identity(4)
    Rot_f[0:3,0:3] = manipulability_f[1]
    S = manipulability_f[0]
    marker = Marker()
    marker.header.frame_id = name+chosenSolver.base_link
    marker.header.stamp = rospy.Time.now()

    marker.pose.position.x = pose[0]
    marker.pose.position.y = pose[1]
    marker.pose.position.z = pose[2]
    quaternion = trans.quaternion_from_matrix( Rot_f)
    #type(pose) = geometry_msgs.msg.Pose
    marker.pose.orientation.x = quaternion[0]
    marker.pose.orientation.y = quaternion[1]
    marker.pose.orientation.z = quaternion[2]
    marker.pose.orientation.w = quaternion[3]

    marker.type = marker.SPHERE
    marker.color.g = 0.7
    marker.color.r = 1.0
    marker.color.a = 0.5
    marker.scale.x = 2*S[0]/500
    marker.scale.y = 2*S[1]/500
    marker.scale.z = 2*S[2]/500

    publish_manip1 = rospy.Publisher(namespace+'panda/manip_force', Marker, queue_size=10)

    publish_manip1.publish(marker)

# main funciton of the class
def panda_manipulability():
    global joint_positions
    global data_time
    rospy.init_node('panda_manipulability', anonymous=True)
    joint_state_topic = rospy.get_param('~joint_state_topic', 'Q_pub')

    rospy.Subscriber(namespace+joint_state_topic, JointState, callback, queue_size= 2)

    data_time = rospy.Time.now()
    #rospy.Subscriber(namespace+"joint_states", JointState, callback)


    rate = rospy.Rate(100) # 10hz #35
    while not rospy.is_shutdown():
        draw_ellipsoids(joint_positions)
        rate.sleep()

# class definition
if __name__ == '__main__':
    try:
        panda_manipulability()
    except rospy.ROSInterruptException:
        pass
