
# to assure compatiblity with Python3
# (and assuming Python3 packages are installed into 
# the path designed by the variable "PYTHON3_LIB_PATH")
import sys
import os
version = float(sys.version.split(" ")[0][0:3])
if version >= 2.7:
    try : 
        python3_path = os.environ["PYTHON3_LIB_PATH"].split(":")
        sys.path = python3_path + sys.path
    except KeyError:
        pass

import numpy as np

# polytope python module
import capacity.capacity_solver as capacity_solver

# minkowski sum
from scipy.spatial import ConvexHull, Delaunay

# URDF parsing an kinematics 
from urdf_parser_py.urdf import URDF
from pykdl_utils.kdl_kinematics import KDLKinematics
import hrl_geom.transformations as trans

import pinocchio as pin
from pinocchio.robot_wrapper import RobotWrapper


class RobotSolver(object):
    # constructor - reading urdf and constructing the robots kinematic chain
    def __init__(self):

        # https://frankaemika.github.io/docs/control_parameters.html
        # maximal torques
        self.t_max = -1
        self.t_min = -self.t_max
        # maximal joint velocities
        self.dq_max = np.zeros((1,1))
        self.dq_min = -self.dq_max
        # maximal joint angles
        self.q_max = np.zeros((1,1))
        self.q_min = np.zeros((1,1))
        # matrix of axis vectors - for polytope search
        self.T_vec = np.zeros((1,1))
        # matrix of 
        self.T_min = np.zeros((1,1))
        self.n = -1
        self.m = 3


    # direct kinematics functions for 7dof
    def dk_position(self, q):
        return self.forward(q)[:3,3]

    def dk_orientation_matrix(self, q):
        return self.forward(q)[0:3,0:3]

    def jacobian_position(self, q):
        return self.jacobian(q)[:3, :]

    def jacobian_pseudo_inv(self, q):
        return np.linalg.pinv(self.jacobian(q))

    def forward(self,q):
        pass

    def jacobian(self,q):
        pass
    # iterative solving of inverse kinematics
    def ik_solve(x_d, q_0, iterations):
        pass

    def gravity_torque(self,q):
        pass

    # velocity manipulability calculation
    def manipulability_velocity(self, q, direction = None):
        # avoid 0 angles
        joint_pos = np.array(q)
        joint_pos[joint_pos==0] = 10**-7

        # jacobian calculation
        if direction is None:
            Jac = self.jacobian_position(q)
        else:
            Jac = np.array(direction).dot(self.jacobian_position(q))
        # use the capacity module
        return capacity_solver.manipulability_velocity(Jac,self.dq_max)

    # force manipulability calculation
    def manipulability_force(self, q, direction = None):
        # avoid 0 angles
        joint_pos = np.array(q)
        joint_pos[joint_pos==0] = 10**-7

        # jacobian calculation
        if direction is None:
            Jac = self.jacobian_position(q)
        else:
            Jac = np.array(direction).dot(self.jacobian_position(q))
        # use the capacity module
        return capacity_solver.manipulability_force(Jac, self.t_max)

    # maximal end effector force
    def force_polytope_intersection(self, q1,q2):
        # jacobian calculation
        Jac1 = self.jacobian_position(q1)
        Jac2 = self.jacobian_position(q2)
        return capacity_solver.force_polytope_intersection_ordered(Jac1,Jac2,self.t_max,self.t_min,self.t_max,self.t_min, self.gravity_torque(q1), self.gravity_torque(q2))


    # maximal end effector force
    def force_polytope_sum(self, q1,q2):
        # jacobian calculation
        Jac1 = self.jacobian_position(q1)
        Jac2 = self.jacobian_position(q2)
        return capacity_solver.force_polytope_sum_ordered(Jac1,Jac2,self.t_max,self.t_min,self.t_max,self.t_min, self.gravity_torque(q1), self.gravity_torque(q2))

    # maximal end effector force
    def force_polytope(self, q, direction = None, force = None):

        # jacobian calculation
        Jac_full = self.jacobian_position(q)
        if direction is not None:
            Jac = np.dot( np.array([direction]), Jac_full )
        else:
            Jac = Jac_full
        
        return capacity_solver.force_polytope_ordered(Jac, self.t_max, self.t_min, self.gravity_torque(q))


class URDFSolverKDL(RobotSolver):
    def __init__(self, description_string, base_link, end_link):
        super(URDFSolverKDL,self).__init__()
        # Physical parameters 
        # URDF file can be parsed instead
        self.robot = URDF.from_parameter_server(description_string)
        self.base_link = base_link
        self.end_link = end_link

        self.kdl_kin = KDLKinematics(self.robot, base_link, end_link)

        self.joint_name_list = self.kdl_kin.get_joint_names()

        # dof, output space
        self.n = len(self.kdl_kin.joint_limits_lower)
        # output space
        self.m = 3

        # https://frankaemika.github.io/docs/control_parameters.html
        # maximal torques
        self.t_max =  np.array([self.kdl_kin.joint_limits_effort]).T #np.array([[87], [87], [87], [87], [12], [12], [12]]) #np.array([[1], [1], [1], [1], [1], [1], [1]])
        self.t_min = -self.t_max
        # maximal joint velocities
        self.dq_max = np.array([self.kdl_kin.joint_limits_velocity]).T
        self.dq_min = -self.dq_max
        # maximal joint angles
        self.q_max = np.array([self.kdl_kin.joint_limits_upper]).T
        self.q_min = np.array([self.kdl_kin.joint_limits_lower]).T
        # matrix of axis vectors - for polytope search
        self.T_vec = np.diagflat(self.t_max-self.t_min)
        # matrix of 
        self.T_min = np.matlib.repmat(self.t_min,1,2**(self.m) )

        # intersection
        self.t_max_int =  np.vstack((self.t_max,self.t_max))
        self.t_min_int = -self.t_max_int
        self.T_vec_int = np.diagflat((self.t_max-self.t_min,self.t_max-self.t_min))
        self.T_min_int = np.matlib.repmat(self.t_min_int,1,2**(self.m) )

    # Transformation matrix of end_link
    def forward(self,q):
        # print(self.joint_name_list)
        return self.kdl_kin.forward(q)

    def jacobian(self,q):
        Jac = self.kdl_kin.jacobian(q)
        #print("kdl :", Jac)
        return Jac

    # iterative solving of inverse kinematics
    def ik_solve(self,x_d, q_0, iterations):
        return  self.kdl_kin.inverse(x_d, q_0)

    def gravity_torque(self,q):
        return np.array([self.kdl_kin.gravity(q)]).T

class URDFSolverPIN(RobotSolver):
    
    def __init__(self, urdf_filename, base_link, end_link):
        """!
        NB : main difference compared to URDFSolverKDL is that
        the class needs the absolute path to the .urdf file, instead
        of the name of the parameter describing the urdf.
        """
        super(URDFSolverPIN,self).__init__()

        self.model = pin.buildModelFromUrdf(urdf_filename)
        self.data = self.model.createData()

        self.base_link = base_link
        self.end_link = end_link

        self.id_base = self.model.getBodyId(self.base_link)

        self.joint_base_id = self.model.frames[self.id_base].parent

        self.id_end = self.model.getBodyId(self.end_link)

        self.joint_end_id = self.model.frames[self.id_end].parent

        # joints between the base_link and the end_link
        joint_ids_base = self.model.supports[self.joint_base_id]
        joint_ids_end = self.model.supports[self.joint_end_id]

        self.joint_ids = joint_ids_end

        size = len(self.joint_ids)
        k = 0
        while k < size:
            if (self.joint_ids[k] in joint_ids_base):
                del(self.joint_ids[k])
                k-=1
                size -= 1
            k+=1

        self.joints_q_id = []

        self.body_name_list = []

        self.joint_name_list = []

        self.n = len(self.joint_ids)
        # output space
        self.m = 3

        self.t_max =  np.zeros((self.n, 1)) #np.array([[87], [87], [87], [87], [12], [12], [12]]) #np.array([[1], [1], [1], [1], [1], [1], [1]])
        # maximal joint velocities
        self.dq_max = np.zeros((self.n, 1))
        # maximal joint angles
        self.q_max = np.zeros((self.n, 1))
        self.q_min = np.zeros((self.n, 1))

        self.q = pin.neutral(self.model)

        # simple test, not useful
        for k in range(len(self.model.frames)):
            jid = self.model.frames[k].parent
            name = self.model.frames[k].name
            if (jid in self.joint_ids and name not in self.joint_name_list):
                self.body_name_list.append(self.model.frames[k].name)
        
        # NB : the order of the joints into self.model.joints and self.q
        # are not the same.
        # Ex : the 2nd joint designed by self.model.joints[1] may not be the joint
        # designed by self.q[1].
        # This is why two objects are used here : 
        # * in order to receive the joint values, we use self.joints_ids, linked to
        # the order of self.model.joints.
        # * in order to compute all necessary operations, we use self.joints_q_id, linked
        # to the order of self.q.

        # first, in order to get all the necessary parameters for the
        # reception of data
        for i in range(len(self.joint_ids)):
            jid = self.joint_ids[i]

            joint = self.model.joints[jid]
            nq = joint.nq
            idx_q = joint.idx_q
            self.joints_q_id.append([idx_q,idx_q+nq])
            self.joint_name_list.append(self.model.names[jid] )            
        
        # put all useful parameters for computation (we suppose the urdf
        # only has joints with 1 DOF).        
        for i in range(len( self.joints_q_id)):

            q_ind = self.joints_q_id[i][0]
            self.t_max[i,0] = self.model.effortLimit[q_ind]
            self.dq_max[i,0] = self.model.velocityLimit[q_ind]
            self.q_max[i,0] = self.model.upperPositionLimit[q_ind]
            self.q_min[i,0] = self.model.lowerPositionLimit[q_ind]

        self.t_min = -self.t_max
        self.dq_min = -self.dq_max

        # dof, output space

        # https://gepettoweb.laas.fr/doc/stack-of-tasks/pinocchio/master/doxygen-html/structpinocchio_1_1ModelTpl.html
        # maximal torques

        # matrix of axis vectors - for polytope search
        self.T_vec = np.diagflat(self.t_max-self.t_min)
        # matrix of 
        self.T_min = np.matlib.repmat(self.t_min,1,2**(self.m) )

        # intersection
        self.t_max_int =  np.vstack((self.t_max,self.t_max))
        self.t_min_int = -self.t_max_int
        self.T_vec_int = np.diagflat((self.t_max-self.t_min,self.t_max-self.t_min))
        self.T_min_int = np.matlib.repmat(self.t_min_int,1,2**(self.m) )

    # Transformation matrix of end_link
    def forward(self,q):

        for i in range(len(self.joints_q_id)):
            nq1,nq2 = self.joints_q_id[i][0], self.joints_q_id[i][1]
            self.q[nq1] = q[i]
        
        pin.forwardKinematics(self.model,self.data,self.q)
        pin.updateFramePlacement(self.model, self.data, self.id_end)
        pin.updateFramePlacement(self.model, self.data, self.id_base)

        base_frame =  self.data.oMf[self.id_base].np

        end_frame =  self.data.oMf[self.id_end].np

        Fp = np.dot( np.linalg.inv(base_frame), end_frame)

        return( Fp )

    def jacobian(self,q):
        """!

        Particularity here : the Jacobian should be expressed on the origin of the referential
        linked to the end_effector, with the same orientation than the axes of the referential of
        the base_link.
        (NB : no translation term is necessary, even if it seems weird). 

        Pbme : best that Pinocchio can do for this is to return the Jacobian of the origin of the end_effector,
        expressed into the referential of the end_effector.
        Solution : apply to the Jacobian the rotation term of the transformation matrix from the base_link
        to the end_link.

        """

        for i in range(len(self.joints_q_id)):
            nq1,nq2 = self.joints_q_id[i][0], self.joints_q_id[i][1]
            self.q[nq1] = q[i]

        Jac = pin.computeFrameJacobian(self.model,self.data,self.q,self.id_end,pin.LOCAL)

        Jac_specific = np.zeros((6,self.n))
        for i in range(len(self.joints_q_id)):
            col = self.joints_q_id[i][0]
            Jac_specific[:,i] = Jac[:,col]
        
        transfo_rot = self.forward(q)[:3,:3]
        Jac_specific[:3,:] = np.dot(transfo_rot, Jac_specific[:3,:])
        Jac_specific[3:,:] = np.dot(transfo_rot, Jac_specific[3:,:])

        return Jac_specific

    # iterative solving of inverse kinematics
    def ik_solve(self,x_d, q_0, iterations):

        for i in range(len(self.joints_q_id)):
            nq1,nq2 = self.joints_q_id[i][0], self.joints_q_id[i][1]
            self.q[nq1] = q_0[i]

        pos = x_d[0:3]
        rot = x_d[3:6]
        Lframe_id = [self.id_end]
        
        rot_mat =  pin.utils.rotate('x',rot[0]) * pin.utils.rotate('y',rot[1]) * pin.utils.rotate('z',rot[2]) 
        pos_mat = np.array([pos[0],pos[1],pos[2]])
        Lcons = [[rot_mat,pos_mat]]

        q, self.data, err = moveListFrame(Lframe_id, Lcons, self.model, self.data, self.q, iterations )
        return(q,err)


    def gravity_torque(self,q):
        for i in range(len(self.joints_q_id)):
            nq1,nq2 = self.joints_q_id[i][0], self.joints_q_id[i][1]
            self.q[nq1] = q[i]

        Grav = np.array([pin.computeGeneralizedGravity(self.model, self.data,self.q)]).T

        Grav_specific = np.zeros((self.n,1))

        for i in range(len(self.joints_q_id)):
            line = self.joints_q_id[i][0]
            Grav_specific[i,0] = Grav[line,0]

        return Grav_specific


def moveListFrame(Lframe_id,Lcons,urdf_model,urdf_data, q0, iterations,verbose=False):
    """!

    Given a list of frames name Lname, a list of constraints to be reached 
    Lcons, a Pinocchio model urdf_model and its associated data object urdf_data, execute
    an Inverse Kinematic operation in order to determine the configuration q to 
    reach the constraints, if these constraints can be achieved. 
    A message is displayed if those constraints are achieved or not. 
    Inspired from https://gepettoweb.laas.fr/doc/stack-of-tasks/pinocchio/master/doxygen-html/md_doc_b-examples_i-inverse-kinematics.html .
    
    @param Lframe_id : List.
    
        List of the id of the frames on which the Inverse Kinematics operation is to be applied.
    
    @param Lcons : List of list.
    
        List of the constraint to be reached by the selected joints (expressed in ground referential).
        One element is organized as follows : 
        [ Rotation matrix, Position matrix ]
        Example : 
        [  pin.utils.rotate('x',3.0)  , np.array([0.08, -0.07, 0.86]) ]
    
    @param urdf_model : Pinocchio model object.
        Pinocchio object containing all the physical description of the loaded URDF model, 
        including kinematic and inertial parameters defining its structure.
        Please check https://gepettoweb.laas.fr/doc/stack-of-tasks/pinocchio/master/doxygen-html/md_doc_a-features_intro.html 
        for more informations.
        
    @param urdf_data : Pinocchio data object.
         Pinocchio object containing all the values of the loaded URDF model which
         are the result of a computation. (joint configuration, velocity, ...).

    @param verbose : Boolean.
        Activate the print functions of the algorithm. Useful for debugging.
        The default is False.

    @return q : numpy.array
        Vector Q from the Inverse Kinematics operation.
    
    @return urdf_data : Pinocchio data object.
         The pinocchio data object updated so the joints have the values set in 
         the Q vector.
    
    @return err : numpy.array (6 x number_of_joints, )
        Final error vector between current and desired joints values.

    """
    L_oMdes = []
    
     
    for k in range(len(Lcons)):
        try:
            oMdes = pin.SE3( Lcons[k][0] , Lcons[k][1])
        except TypeError:
            oMdes = Lcons[k]
        L_oMdes.append(oMdes) 

    q      = q0

    # minimum difference between current and desired joints values to stop the
    # Inverse Kinematics operation.
    eps    = 1e-4
    # the maximum number of iterations of the Inverse Kinematics operation.
    IT_MAX = iterations
    # time step
    DT     = 1e-1
    # regularization value
    damp   = 1e-6
    #print(L_oMdes[0].toActionMatrix() )
    # print( L_oMdes[0].actInv(urdf_data.oMi[Lframe_id[0]] ) )

    # M1 = L_oMdes[0]
    # M2 = urdf_data.oMi[Lframe_id[0]]

    # R3 = np.dot(M1.rotation.T,M2.rotation)

    # print("R3 : ", R3)

    i=0
    while True:
          nb_cons = len(Lframe_id)
          # forward kinematics to the urdf model
          pin.forwardKinematics(urdf_model,urdf_data,q)
          
          # check the difference between current and desired joints values.
          err = np.zeros((1,1))
          for k in range(len(L_oMdes)):
            dMi= L_oMdes[k].actInv(urdf_data.oMf[Lframe_id[k]] )
            #print(dMi)
            if err.shape == (1,1):
                err = pin.log(dMi).vector
            else:
                err = np.hstack( ( err,  pin.log(dMi).vector ) )
         
          if np.norm(err) < eps:
              success = True
              break
          if i >= IT_MAX:
              success = False
              break
    
          # get the Jacobian of each of the joints concerned by the Inverse Kinematics
          # operation.
          J = np.zeros((1,1))
          for k in range(len(Lframe_id)):
              Jk = pin.computeFrameJacobian(urdf_model,urdf_data,q,Lframe_id[k])
              if J.shape == (1,1):
                  J = Jk
              else:
                  J = np.vstack( (J, Jk) )
         
          # get the velocity vector.
          v = - J.T.dot( np.solve(J.dot(J.T) + damp * np.eye(6*nb_cons), err)           )
          
          # get the vector Q according to the velocity vector.
          q = pin.integrate(urdf_model,q,v*DT)
              
          if not i % 10 and verbose:
              print('%d: error = %s' % (i, err.T))
          i += 1
    
    if verbose:
        if success :
            print("Convergence achieved!")
        else:
            print("\nWarning: the iterative algorithm has not reached convergence to the desired precision")
                
        print('\nresult: %s' % q.flatten().tolist())
        print('\nfinal error: %s' % err.T)

    return(q,urdf_data,err)

def printConfig(urdf_model,urdf_data):
    """!
    
    Displays the position and orientation of each of the joints of the .urdf
    model, expressed into the global referential associated with the .urdf. 
    
    @param urdf_model : Pinocchio model object.
        Pinocchio object containing all the physical description of the loaded URDF model, 
        including kinematic and inertial parameters defining its structure.
        Please check https://gepettoweb.laas.fr/doc/stack-of-tasks/pinocchio/master/doxygen-html/md_doc_a-features_intro.html 
        for more informations.
        
    @param urdf_data : Pinocchio data object.
         Pinocchio object containing all the values of the loaded URDF model which
         are the result of a computation. (joint configuration, velocity, ...).

    @return None.

    """    
    print("----- Translation -----")    
    for name, oMi in zip(urdf_model.names, urdf_data.oMi):
        print(("{:<24} : {: .4f} {: .4f} {: .4f}"
              .format( name, *oMi.translation.T.flat )))
    print("----- Rotation -----")
    for name, oMi in zip(urdf_model.names, urdf_data.oMi):
        print(("{:<24} : {: .4f} {: .4f} {: .4f} "
            .format(name, *list(trans.euler_from_rot_mat(oMi.rotation)))) )

def printQ(urdf_model,q):
    """!
    
    Displays the value of each of the articulations of the .urdf model.

    @param urdf_model : Pinocchio model object.
        Pinocchio object containing all the physical description of the loaded URDF model, 
        including kinematic and inertial parameters defining its structure.
        Please check https://gepettoweb.laas.fr/doc/stack-of-tasks/pinocchio/master/doxygen-html/md_doc_a-features_intro.html 
        for more informations.
        
    @param q : numpy.array
        Vector Q associated with the .urdf model. 

    @return None.

    """
    print("----- Q from URDF -----")    
    for joint,name in zip(urdf_model.joints,urdf_model.names):
        nq = joint.nq
        idx_q = joint.idx_q
        q_joint = q[idx_q:idx_q+nq]
        print(" %s : "%name, q_joint)
